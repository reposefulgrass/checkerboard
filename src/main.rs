extern crate image;

use std::fs::File;
use image::ColorType;
use image::png::PNGEncoder;

use Color::{White, Black};

enum Color {
    White,
    Black
}

impl Color {
    fn opposite_color (&self) -> Color {
        match &self {
            White => Black,
            Black => White,
        }
    }

    fn to_value (&self) -> u8 {
        match &self {
            White => 0 as u8,
            Black => 255 as u8
        }
    }
}

struct Bound {
    width: u32,
    height: u32
}

fn generate_checkerboard (bounds: Bound, tiles: u32) -> Vec<u8> {
    let tile_width  = bounds.width / tiles as usize;
    let tile_height = bounds.height / tiles as usize;

    let mut pixels = vec![0; bounds.width * bounds.height];

    let mut color;
    for row in 0 .. bounds.height {
        // starting color of each row
        if ((row / tile_height) % 2) == 0 {
            color = White; 
        } else {
            color = Black;
        }

        for column in 0 .. bounds.width {
            // alternate the color when it reaches a new tile
            if (column % tile_width) == 0 {
                color = color.opposite_color();
            }
            pixels[row * bounds.width + column] = color.to_value();
        }
    }

    pixels
}

fn write_image (filename: &str, pixels: &[u8], bounds: Bound)
    -> Result<(), std::io::Error> {

    let output = File::create(filename)?;
    let encoder = PNGEncoder::new(output);
    encoder.encode(&pixels, bounds.width, bounds.height, ColorType::Gray(8))?;
    Ok(())
}

fn main() {
    let bounds = Bound { width: 640, height: 640 };
    let tiles = 8;

    let pixels = generate_checkerboard(bounds, tiles);
    write_image("checkerboard.png", &pixels, bounds)
        .expect("Error writing PNG file");
}



